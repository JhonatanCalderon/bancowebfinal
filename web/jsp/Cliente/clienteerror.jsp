
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ERROR CLIENTE</title>
    </head>
    <body>
        <% 
            String error = (String) request.getAttribute("error");
        %>
        <h1><%= error %></h1>
        
        <a  href="html/Cliente/RegistrarCliente.html">Registrar Cliente</a>
        <a  href="html/Cliente/">Modificar Cliente</a>
        <a  href="html/Cliente/EliminarCliente.html">Eliminar Cliente</a>
        
    </body>
</html>
