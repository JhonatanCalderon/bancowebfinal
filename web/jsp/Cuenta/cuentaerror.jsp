<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ERROR CUENTA</title>
    </head>
    <body>
        <% 
            String error = (String) request.getAttribute("error");
        %>
        <h1><%= error %></h1>
        
        <a  href="././jsp/Cuenta/RegistrarCuenta.jsp">Registrar Cuenta</a>
        <a  href="././html/Cuenta/EliminarCuenta.html">Eliminar Cuenta</a>
    </body>
</html>
