/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import DAO.ClienteJpaController;
import DAO.Conexion;
import DTO.Cliente;
import DTO.Cuenta;
import DTO.TipoMovimiento;
import Negocio.Banco;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.apache.tomcat.util.buf.StringUtils.join;

/**
 *
 * @author SEDE LA PERLA
 */
public class Informe extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String accion = request.getParameter("generar");
        Conexion cox = Conexion.getConexion();    
        ClienteJpaController cl = new ClienteJpaController(cox.getBd());
        try {
             
            if(accion.equals("Registrar")){
                
                
                List<Cliente> l = cl.findClienteEntities();
                String cadena = l.toString();
                 //List<String> intList = Arrays.asList(l);
                    //redireccion a exitoso
                    System.err.println(cadena);
                    request.setAttribute("msj","PRUEBA DEL INFORME");
                    request.getRequestDispatcher("./jsp/Informe/informeexitoso.jsp").forward(request, response);
                         
            }
            
        } catch (Exception e) {
            //redirecciona a error
            request.setAttribute("error",cl.findClienteEntities());
            request.getRequestDispatcher("./jsp/Informe/informeexitoso.jsp").forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void join(List<Cliente> l) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
