/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import DTO.Cuenta;
import DTO.TipoMovimiento;
import Negocio.Banco;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author SEDE LA PERLA
 */
public class Consignacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
            String accion = request.getParameter("Consignacion");
            TipoMovimiento tp = new TipoMovimiento();
        try {
             
            if(accion.equals("Registrar")){
                Banco banco = new Banco();
                int cuentaDestino = Integer.parseInt(request.getParameter("nrocuenta"));
                int monto = Integer.parseInt(request.getParameter("monto"));
                int tipoM = 1;
                Cuenta cta = new Cuenta(cuentaDestino);
                //cta.equals(cuentaDestino);
                
                if(banco.registrarConsignacion(cuentaDestino,monto,tipoM,accion)){
                    //redireccion a exitoso
                    request.setAttribute("msj", "Consignacion exitosa ");
                    request.getRequestDispatcher("./jsp/Operacion/operacionexitosa.jsp").forward(request, response);
                }
                
            }
            
        } catch (Exception e) {
            //redirecciona a error
            request.setAttribute("error", "La consignación no se realizó");
            request.getRequestDispatcher("./jsp/Operacion/operacionerror.jsp").forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
