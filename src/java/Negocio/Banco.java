/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import DAO.CajeroJpaController;
import DAO.ClienteJpaController;
import DAO.Conexion;
import DAO.CuentaJpaController;
import DAO.MovimientoJpaController;
import DAO.TipoJpaController;
import DAO.TipoMovimientoJpaController;
import DAO.exceptions.IllegalOrphanException;
import DAO.exceptions.NonexistentEntityException;
import DTO.Cajero;
import DTO.Cliente;
import DTO.Cuenta;
import DTO.Movimiento;
import DTO.Tipo;
import DTO.TipoMovimiento;
import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author SEDE LA PERLA
 */
public class Banco {
//    public String usuario = "cajerobanco";
    //public int cont = 0;
    
    public boolean insertarCliente(int cedula,String nombre, String email, String direccion, int telefono, String fecha) throws Exception{
        Cliente cl = new Cliente();
        cl.setCedula(cedula);
        cl.setNombre(nombre);
        cl.setEmail(email);
        cl.setDircorrespondencia(direccion);
        cl.setTelefono(telefono);
        cl.setFechanacimiento(crearFecha(fecha));
        
        Conexion cox = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(cox.getBd());
        
        try {
            clienteDAO.create(cl);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public boolean insertarCuenta(int nroCuenta, double saldo, Date fecha, int cedula, int tipo){
        Conexion cox = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(cox.getBd());
        CuentaJpaController cuentaDAO = new CuentaJpaController(cox.getBd());
        Cliente cli = clienteDAO.findCliente(cedula);
        
        
        TipoJpaController tipoDAO = new TipoJpaController(cox.getBd());
        Tipo tp = tipoDAO.findTipo(tipo);
        
        Cuenta cta = new Cuenta();
        
        cta.setNroCuenta(nroCuenta);
        cta.setSaldo(saldo);
        cta.setFechacreacion(fecha);
        cta.setCedula(cli);
        cta.setTipo(tp);
        
        try {
            cuentaDAO.create(cta);
            return true;
        } catch (Exception e) {
            return false;
        }
        
    }
    public boolean eliminarCliente(Integer cedula) throws IllegalOrphanException, NonexistentEntityException{
        Conexion cox = Conexion.getConexion();
        ClienteJpaController clienteDAO = new ClienteJpaController(cox.getBd());
        try{
            clienteDAO.destroy(cedula);
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
    
    public boolean eliminarCuenta(Integer nroCuenta) throws IllegalOrphanException, NonexistentEntityException{
        Conexion cox = Conexion.getConexion();
        CuentaJpaController cuentaDAO = new CuentaJpaController(cox.getBd());
        try{
            cuentaDAO.destroy(nroCuenta);
            return true;
        }
        catch(Exception e){
            return false;
        }
    }
    private Date crearFecha(String fecha) {
        String fechas[] = fecha.split("-");
        int agno = Integer.parseInt(fechas[0]);
        int mes = Integer.parseInt(fechas[1]);
        int dia = Integer.parseInt(fechas[2]);
        return new Date(agno, mes, dia);
    }
    public boolean validarCajero(String usuario, String contrasenia) {
        Conexion cox = Conexion.getConexion();
        Cajero ca;
        CajeroJpaController cajeroDAO = new CajeroJpaController(cox.getBd());
        ca = cajeroDAO.findCajero(usuario);
        if(ca.getClave().equals(contrasenia)  ){
             return true; 
        }
       return false; 
}
    public boolean registrarConsignacion(int cuentaDestino,int monto,int tipoM, String accion) throws NonexistentEntityException, Exception{
        Conexion cox = Conexion.getConexion();
        TipoMovimientoJpaController tipoDAO = new TipoMovimientoJpaController(cox.getBd());
        MovimientoJpaController movDAO = new MovimientoJpaController(cox.getBd());
        CuentaJpaController ctaDAO = new CuentaJpaController(cox.getBd());
        TipoMovimiento tipoMov = tipoDAO.findTipoMovimiento(tipoM);
        Movimiento mov = new Movimiento();
        
        //tipoMov.setDescripcion(descripcion);
        //tipoMov.setId(1);
        Date fecha = java.sql.Date.valueOf(LocalDate.now());
        
        mov.setId(1);
        mov.setFecha(fecha);
        mov.setValor(monto);
        mov.setNumCuenta(cuentaDestino);
        mov.setIdTipoMovimiento(tipoMov);
        
        Cuenta x = ctaDAO.findCuenta(cuentaDestino);
        x.setSaldo(x.getSaldo()+monto);
        
        ctaDAO.edit(x);
        
        try {
            movDAO.create(mov);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean insertarRetiro(int monto,int cuentaOrigen, String accion,int ctm)throws NonexistentEntityException, Exception{
    Conexion cox= Conexion.getConexion();
    TipoMovimientoJpaController tipoDAO = new TipoMovimientoJpaController(cox.getBd());
    MovimientoJpaController movDAO = new MovimientoJpaController(cox.getBd());
    CuentaJpaController cuentaDAO = new CuentaJpaController(cox.getBd());
    TipoMovimiento tm = tipoDAO.findTipoMovimiento(ctm);
    Cuenta cue = cuentaDAO.findCuenta(cuentaOrigen);
    cue.setSaldo(cue.getSaldo()-monto);
    //tm.setDescripcion(accion);
    //tm.setId(2);
    Date fecha = java.sql.Date.valueOf(LocalDate.now());
    
    Movimiento mo = new Movimiento();
    mo.setId(1);
    mo.setFecha(fecha);
    mo.setValor(monto);
    mo.setNroCuenta(cue);
    mo.setIdTipoMovimiento(tm);
        try {
            cuentaDAO.edit(cue);
            movDAO.create(mo);
            return true;
        } catch (Exception e) {
            return false;
        }
    
    
    
            
    }
    public static void main(String[] args) throws Exception {
        
        //Cliente nuevo = new Cliente(1010115060,"JHONATAN",new Date(2000,06,8),"barrio",310352,"jhonatn@hotmail.com");
        
    }
    
    
    
    
    
}
