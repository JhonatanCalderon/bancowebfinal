<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Cuenta Exitosa</title>
    </head>
    <body>
        <% 
            String msj = (String) request.getAttribute("msj");
        %>
        <h1><%= msj %></h1>
        
        <a  href="././jsp/Cuenta/RegistrarCuenta.jsp">Registrar Cuenta</a>
        <a  href="././html/Cuenta/EliminarCuenta.html">Eliminar Cuenta</a>
    </body>
</html>
