<%@page import="java.util.List"%>
<%@page import="DTO.Cliente"%>
<%@page import="DAO.ClienteJpaController"%>
<%@page import="DAO.Conexion"%>
<%@page import="java.util.Iterator"%>
<%@page import="Negocio.Banco"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Registro Cuenta</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../../css/estilo.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <p class="register-title">REGISTRAR CUENTA</p>
        <a class="btnStyle" href="../../html/Cuenta/Opciones.html">Ir atrás</a>
        
        <form name="Registrar" action="../../RegistroCuenta.do" class="register">

            <p>Nro. Cuenta (10 dígitos):<input class="register-input" maxlength="10" type="text" name="nrocuenta" value="" /></p>    
            <h3>Tipo de Cuenta</h3>
            <select name="tipoSelect">
                <option> Elegir tipo </option>
                <option value="1"> Cuenta Ahorros</option>
                <option value="2"> Cuenta Corriente </option>           
            </select>
            <h3>Saldo</h3>
            <hr>
            <br>
            <input class="register-input" type="number" name="saldo" value="" />
            <h3>Cliente</h3>            
            <select name="cedulaSelect">
                <option> Seleccionar Cliente </option>
                <%
                    Conexion cox = Conexion.getConexion();
                    ClienteJpaController clienteDAO = new ClienteJpaController(cox.getBd());
                    List<Cliente> clientes = clienteDAO.findClienteEntities();
                   
                    for(Cliente inf: clientes){
                        Integer cedula = inf.getCedula();
                        String nombre =  inf.getNombre();
                %>      
                <option value="<%=cedula%>"> <%= cedula + " | " + nombre%> </option>
                <%   }
                %>
            </select>
            <hr>           

            <input type="submit" value="Registrar" class="register-button" name="registrarCta"/>

        </form>
        
    </body>
</html>

